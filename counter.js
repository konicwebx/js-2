function increment() {
  var el = document.getElementById("count");
  var current = parseInt(el.innerHTML); // current - 0
  var final = current + 1; // final - 1
  el.innerHTML = final;
}

function decrement() {
  var el = document.getElementById("count");
  var current = parseInt(el.innerHTML); // current - 0
  var final = current - 1; // final - 1
  el.innerHTML = final;
}

function incdec(change) {
  var el = document.getElementById("count");
  var current = parseInt(el.innerHTML);
  var final = current + change;
  el.innerHTML = final;
}
